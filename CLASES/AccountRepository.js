var ArrayList = require("arraylist-js");
var AccountRepository = function(id, method, action) { // implements Composite, FormItem
    //...
  };
  function AccountRepository() {
      this.lista = new ArrayList();
      this.listaConnects = new ArrayList();
  }
  
  AccountRepository.prototype.getLista = function() {
    //DEVUELVE EL ARRAYLIST DE ACCOUNT QUE SE LLAMA LISTA
  };
  AccountRepository.prototype.save = function(account) {
    //GUARDA LAS CUENTAS EN LA VARIABLE LISTA
  };
  AccountRepository.prototype.getListaConnects = function() {
    //DEVUELVE EL ARRAYLIST DE ACCOUNT QUE SE LLAMA LISTACONNECTS
  };
  
  AccountRepository.prototype.setListaConnects = function(account) {
    //GUARDA LAS CUENTAS EN LA VARIABLE LISTACONNECTS
  };

  AccountRepository.prototype.findAccount = function(email) {
    //RETORNA LA CUENTA ASOCIADA AL CORREO
  };

  AccountRepository.prototype.findRol = function(account) {
    //DEVUELVE UN OBJETO DE TIPO ROL
  };

  AccountRepository.prototype.isBloqueado = function(account) {
    //DEVUELVE UN BOOLEAN
  };

  AccountRepository.prototype.remove = function(account) {
    //REMUEVE LA CUENTA DE LISTACONNECTS
  };
  module.exports = AccountRepository;