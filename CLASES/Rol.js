class Rol {
    constructor(descripcion, estado, id) {
        this.descripcion = descripcion; //STRING
        this.estado = estado;//BOOLEAN
        this.idRol = id;//ENTERO
    }

    getIdRol() {
        return this.idRol;
    }

    getDescripcion() {
        return this.descripcion;
    }

    setIdRol(id) {
        this.idRol = id;
    }

    setDescripcion(descripcion) {
        this.descripcion = descripcion;
    }

    getEstado() {
        return this.estado;
    }

    setEstado(estado) {
        this.estado = estado;
    }    
}

module.exports = Rol;