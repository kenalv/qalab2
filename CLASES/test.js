const Account = require('./Account');
const AccountRepo = require('./AccountRepository');
const Rol = require('./Rol');
const LoginService = require('./LoginService');

var ArrayList = require("arraylist-js");
const mochito = require('mochito');
const expect = require('expect');


let user;
let rol;
let accrepo;
let lista = new ArrayList();
let listaConectados = new ArrayList();
describe('Creando accounts', () => {
   beforeEach(() => {
    user = new Account();
    user.setEmail("kenalv@gmail.com");
    user.setFullname("kenneth");
    user.setPassword("Kae1234567");
    user.setUser("kenalv");
 //   lista.push(user);
    accrepo = mochito.mock(AccountRepo);
    accrepo.lista = lista;
    accrepo.listaConectados = lista;
    accrepo.save(user);
    accrepo.setListaConnects(user);
   
   }); 

   it('Usuario conectado', () => {
       mochito.when(accrepo).findAccount("kenalv@gmail.com").thenReturn(user);
       expect(accrepo.findAccount("kenalv@gmail.com")).toBe(user);
   });

   it('Agrega nueva cuenta', () => {
       let user2 = new Account();
       user2.setEmail("kae@gmail.com");
       user2.setFullname("kenn");
       user2.setPassword("Kae1234567");
       user2.setUser("kentoz");
       mochito.when(accrepo).save(user2).then(accrepo.lista.push(user2));
       accrepo.save(user2);
       mochito.when(accrepo).getLista().thenReturn(accrepo.lista);
       expect(accrepo.getLista().size()).toBe(1);
   });
  

});

