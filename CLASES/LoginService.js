class LoginService {

    constructor(accountRepository) {
        this.accountRepository = accountRepository;
    }

    connect(user, password) {
        var lista = this.accountRepository.getLista();
        for (var i = 0; i < lista.size(); i++) {
            var a = lista.get(i);
            if (a.getUser() === user && a.getPassword() === password) {
                if (this.accountRepository.isBloqueado(a) === false) {
                    this.accountRepository.setListaConnects(a);
                    a.setIntentosFallidos(0);
                    return true;
                } 
                else {
                    throw new Error("Usuario bloqueado");
                }
            } else if (a.getUser() === user && !(a.getPassword() === password) && this.accountRepository.isBloqueado(a)===false) {
                a.setIntentosFallidos(a.getIntentosFallidos() + 1);
                if (a.getIntentosFallidos() >= 3) {
                    a.setBloqueado(true);
                }
            }
        }
        return false;
    }

    desconnect(user) {
        var lista = this.accountRepository.getListaConnects();
        for (var i = 0; i < lista.size(); i++) {
            var a = lista.get(i);
            if (a.getUser() === user) {
                this.accountRepository.remove(a);
                return true;
            }
        }
        return false;
    }

    isConnect(email) {
        var account = this.accountRepository.findAccount(email);
        if(account ===  undefined || account === null)
            return false;
        return true;
    }

    isUserInRole(email, id) {
        var a = this.accountRepository.findAccount(email);
        return id === this.accountRepository.findRol(a).getIdRol();
    }
}

module.exports = LoginService;