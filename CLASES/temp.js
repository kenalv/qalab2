const Sucursal = require('./Sucursal');
const Cliente = require('./Cliente');
const Cuenta = require('./Cuenta');
const FondosInsuficientesException = require('./FondosInsuficientesException');
const ValoresNegativosException = require('./ValoresNegativosException')

const mochito = require('mochito');
const expect = require('expect');

let sucursal;
let cliente;
let cuenta;

let numeroCuenta = 1234;
let balance = 10000;
let withdrawlamount2000 = 200000;

describe('Creando productos', () => {
    beforeEach(() => {
        sucursal = new Sucursal('Florencia', 'Florencia');
        cliente = new Cliente('Marlen', 'Treviño Villalobos', '14-05-1982', '8895-0000', 'Florencia', 'marlen@gmail.com');
        cuenta = mochito.mock(Cuenta);
        sucursal.setClientes(cliente);
        cuenta.setNumCuenta(numeroCuenta);
        cuenta.setCantidadDinero(balance);
        cliente.setCuentas(cuenta);
    });

    it('Saldo de la cuenta', () => {
        mochito.when(cuenta).getCantidadDinero().thenReturn(balance);
        expect(cuenta.getCantidadDinero()).toBe(balance);
    });

    it('Agregar nueva cuenta', () => {
        let cuenta2 = mochito.mock(Cuenta);
        cliente.setCuentas(cuenta2);
        expect(cliente.getCuentas().size()).toBe(2);
    });

    it('Retirar monto valido', () => {
        let balanceAmount3000 = 300000;
        cuenta.setCantidadDinero(balanceAmount3000);
        mochito.when(cuenta).getCantidadDinero().thenReturn(balanceAmount3000);
        mochito.when(cuenta).getNumCuenta().thenReturn(numeroCuenta);
        mochito.when(cuenta).retirar(withdrawlamount2000).thenReturn(balance);
        
        let saldo = cliente.retirar(withdrawlamount2000, numeroCuenta);
        expect(saldo).toBe(balance);
    });

    it('Retirar mas de lo permitido', () => {
        mochito.when(cuenta).getCantidadDinero().thenReturn(balance);
        mochito.when(cuenta).getNumCuenta().thenReturn(numeroCuenta);
        expect(() => {
            cliente.retirar(withdrawlamount2000, numeroCuenta);

        }).toThrow(FondosInsuficientesException, 'Fondos insuficientes');
        mochito.verify(cuenta, mochito.times(0)).retirar(withdrawlamount2000);
    });

    it('Depositar monto negativo', () => {
        mochito.when(cuenta).setCantidadDinero(-100000).thenThrow(new ValoresNegativosException);
        expect(() => {
            cuenta.setCantidadDinero(-100000)
        }).toThrow(ValoresNegativosException, 'Valores negativos');
    });
});

